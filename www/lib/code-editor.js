(function ($, window, document) {

    const pluginName = 'codeEditor';

    function CodeEditor(element, options) {
        if (element.prop('tagName').toLowerCase() !== 'textarea') {
            console.error('ACE Editor can be initialized on textarea element only');
            return;
        }
        this.element = element.data('codeEditor', this);
        this.options = $.extend(true, this.defaults, options);
        this.init();
    }

    CodeEditor.prototype = {
        toolbar: null,
        defaults: {

        },

        init: function () {
            const self = this;

            // ACE init
            const editorWrapperEl = $('<div>').attr('class', 'editor-wrapper border').insertBefore(self.element);
            // TODO styles
            self.toolbar = $('<div style="text-align: right; padding: 5px;">')
                .attr('class', 'editor-toolbar').prependTo(editorWrapperEl);
            const editorEl = self.element.clone().appendTo(editorWrapperEl).hide();

            const editorReadonly = self.element.attr('readonly') || self.element.attr('disabled');
            const editor = ace.edit(editorEl[0], {
                fontFamily: 'consolas',
                fontSize: '15px',
                minLines: 3,
                maxLines: 40,
                // wrap: true,
                autoScrollEditorIntoView: true,
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true,
                readOnly: editorReadonly,
                mode: self.options.mode ? 'ace/mode/' + self.options.mode : null,
                theme: self.options.theme ? 'ace/theme/' + self.options.theme : null,
            });

            // ACE and textarea bind
            editor.on('change', function () {
                self.element.text(editor.session.getValue());
                self.element[0].dispatchEvent(new Event('change'));
            });
            self.element.data('ace', editor);
            self.element.hide();

            return self;
        },

        addToolbarButton: function (title, icon, handler) {
            const self = this;

            $('<button type="button" class="btn btn-sm btn-secondary">\n\
                <i class="'+ icon +'"></i>\n\
            </button>').attr('title', title).on('click', handler).appendTo(self.toolbar);
        },

        addCopyToolbarButton: function () {
            const self = this;

            self.addToolbarButton('Copy to clipboard', 'far fa-copy', function () {
                const isDisabled = self.element.prop('disabled');
                self.element.prop('disabled', false).show().select();
                document.execCommand('copy');
                self.element.prop('disabled', isDisabled).hide();
            });
        },
    };
    const plugin = function (options) {
        const plugin = new CodeEditor(this, options);
        return this;
    };
    $.fn[pluginName] = plugin;

})(jQuery, window, document);
