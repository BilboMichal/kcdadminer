// Extending methods from original Live-Form-validation.js library
//
(function () {
    const addError = LiveForm.addError;
    LiveForm.addError = function (el, message) {
        addError.call(LiveForm, el, message);
        $(el).trigger('liveForm:addError', message);
    };

    const removeError = LiveForm.removeError;
    LiveForm.removeError = function (el) {
        removeError.call(LiveForm, el);
        $(el).trigger('liveForm:removeError');
    };

    const netteValidateControl = Nette.validateControl;
    Nette.validateControl = function (el, rules, onlyCheck, value, emptyOptional) {
        let result = netteValidateControl.call(Nette, el, rules, onlyCheck, value, emptyOptional);
        if (result && typeof $(el).data('netteValidator') === 'function') {
            result = $(el).data('netteValidator').call(this, el, onlyCheck, value);
        }
        return result;
    };
})();