const App = function (options) {
};

App.prototype = {
    init: function () {
        const self = this;

        self.initGrids();
        self.initForms();
        self.initNette();
        self.initControls();
    },

    initGrids: function () {
        const self = this;

        $('a[data-copy-clipboard]').on('click', function () {
            const rowId = $(this).parents('*[data-id]').attr('data-id');
            const inputTemp = $('<input>').val(rowId).appendTo($(this)).select();
            document.execCommand('copy');
            inputTemp.remove();
        });
    },

    initForms: function () {
        const self = this;

        $('form[data-leave-confirm]').areYouSure();

        LiveForm.setOptions({
            messageErrorPrefix: 'Error: ',
            wait: 500,
            showMessageClassOnParent: false,
            controlErrorClass: 'is-invalid',
            controlValidClass: 'is-valid',
            showValid: true
        });
    },

    initNette: function () {
        const self = this;

        $.nette.init();
    },

    initControls: function (container) {
        const self = this;

        if (typeof container === 'undefined') {
            container = document;
        }
        $(container).find('*[data-control]').each(function () {
            const control = this;
            const controlId = control.getAttribute('id');
            const controlTypes = control.getAttribute('data-control');
            const controlOptions = this.hasAttribute('data-control-options') ? JSON.parse(control.getAttribute('data-control-options')) : {};

            controlTypes.split('|').forEach(function (controlType) {
                switch (controlType) {
                    case 'select2':
                        $(control).select2({
                            theme: 'bootstrap-5',
                        });
                        break;
                    case 'code-editor':
                        self.initCodeEditor(control, controlOptions);
                        break;
                    default:
                        console.warn('Invalid control type <' + controlType + '>');

                }
            });
        });
    },

    /**
     *
     * @param {object} control
     * @returns {void}
     */
    initCodeEditor: function (control, controlOptions) {
        const controlEl = $(control).codeEditor(controlOptions);

        controlEl.data('netteValidator', function (el) {
            const editor = $(el).data('ace');
            if (editor) {
                const annotations = editor.session.getAnnotations();
                if (annotations.length === 0) {
                    LiveForm.removeError(el);
                    return true;
                } else {
                    LiveForm.addError(el, 'Error in code');
                    return false;
                }
            }

            return true;
        });

        const codeEditor = controlEl.data('codeEditor');
//        codeEditor.addFullscreenToolbarButton();
        codeEditor.addCopyToolbarButton();

    },

}

// --------------------------------------------------------------------

const app = new App;
$(function () {
    app.init();
});
