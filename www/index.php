<?php
declare(strict_types=1);

use Config\Bootstrap;
use Nette\Application\Application;

require __DIR__ . '/../vendor/autoload.php';

$container = Bootstrap::boot()->createContainer();

$application = $container->getByType(Application::class);
$application->run();
