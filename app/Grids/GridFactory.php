<?php
declare(strict_types=1);

namespace App\Grids;

use Ublaboo\DataGrid\DataGrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class GridFactory
{

    public function create(): DataGrid
    {
        $grid = new DataGrid;
        $grid->setDefaultPerPage(50);
        $grid->setRememberState(false);

        return $grid;
    }
}