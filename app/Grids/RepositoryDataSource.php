<?php
declare(strict_types=1);

namespace App\Grids;

use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Repository\IRepository;
use Ublaboo\DataGrid\DataSource\IDataSource;
use Ublaboo\DataGrid\Filter\Filter;
use Ublaboo\DataGrid\Utils\Sorting;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class RepositoryDataSource implements IDataSource
{
    private ICollection $collection;
    private $condsBuilder;

    public function __construct(private readonly IRepository $repository, callable $condsBuilder = null)
    {
        $this->condsBuilder = $condsBuilder;
    }

    public function filter(array $filters): void
    {
        $conds = [];
        foreach ($filters as $name => $filter) {
            /* @var $filter Filter */
            $value = $filter->getValue();
            if ($value !== null) {
                $conds[$name] = $value;
            }
        }

        if (isset($this->condsBuilder)) {
            $conds = ($this->condsBuilder)($conds);
        }
        $this->collection = $conds ? $this->repository->findBy($conds) : $this->repository->findAll();
    }

    public function filterOne(array $condition): self
    {
        
    }

    public function getCount(): int
    {
        return $this->collection->count();
    }

    public function getData(): iterable
    {
        return $this->collection;
    }

    public function limit(int $offset, int $limit): self
    {
        $this->collection = $this->collection->limitBy(limit: $limit, offset: $offset);
        return $this;
    }

    public function sort(Sorting $sorting): self
    {

//        bdump($sorting, 'SORTING');
//        $this->collection->orderBy($expression);
        return $this;
    }
}