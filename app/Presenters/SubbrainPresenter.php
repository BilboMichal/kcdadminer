<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use App\Forms\Hydrator;
use App\Model\Entity\Subbrain;
use App\Model\Entity\SubbrainRelation;
use League\Flysystem\FilesystemReader;
use Ublaboo\DataGrid\DataGrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SubbrainPresenter extends BasePresenter
{

    use UuidValidationTrait;
    //
    private Subbrain $subbrain;

    protected function setupRepository()
    {
        $this->repository = $this->model->subbrain;
    }

    public function actionList()
    {
        $this->template->title = $this->getShortName();
        $this->addBreadcrumb($this->template->title);
    }

    public function actionEdit(string $id = null)
    {
        $this->checkUuid($id);
        $this->subbrain = $this->retrieveEntity($id);

        $this->addBreadcrumb($this->getShortName(), 'list');

        $title = $this->subbrain->isPersisted() ? $this->subbrain->subbrainName : '-- new --';
        $this->addBreadcrumb($title, 'this');
        $this->template->title = $title;
    }

    public function handleDelete(string $id)
    {
        $entity = $this->retrieveEntity($id);

        $this->repository->removeAndFlush($entity);

        $this->flashMessage($this->messageRemoveEntity($entity), self::FLASH_SUCCESS);
        $this->redirect('this');
    }

    private function readRelationFileData(string $fs, SubbrainRelation $relation): string
    {
        $fsPath = $fs . '://' . $relation->filePath;
        return $this->mountManager->has($fsPath) ? $this->mountManager->read($fsPath) : '';
    }

    private function updateRelationFileData(string $fs, SubbrainRelation $relation, string $data): void
    {
        $fsPath = $fs . '://' . $relation->filePath;

        if ($data) {
            $this->mountManager->write($fsPath, $data);
        } elseif ($this->mountManager->has($fsPath)) {
            $this->mountManager->delete($fsPath);
        }
    }

    protected function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('subbrainName', 'Name')
            ->setRequired();
        $form->addCheckbox('alwaysActive', 'Always active');

        $subbrainTypes = $this->model->subbrainType->findAll()->fetchPairs('subbrainTypeId', 'subbrainTypeName');

        /*$typeInput = $this->createFormInput('select2', 'Type', $subbrainTypes);
        $typeInput->setPrompt('-- no type --');
        $form->addComponent($typeInput, 'subbrainType');*/
        $typeInput = $form->addSelect('subbrainType', 'type', $subbrainTypes);
        $typeInput->setPrompt('-- no type --');

        $form->addComponent(
            component: $this->createFormInput('brains', 'Brains')->setOption('hydrateIn', 'brainsIds'),
            name: 'brains'
        );

        $subbrainTypesReverse = array_flip(array_map('lcfirst', $subbrainTypes));

        // Subbrain relations containers -> each form container represent single relation
        // Builds from entity metadata
        foreach ($this->subbrain->getRelationMetadata() as $relationName => $relationMetadata) {
            $relationContainer = $form->addContainer($relationName)
                ->setOption('hydrateOut', Hydrator::HYDRATE_OFF)
                ->setOption('mappedType', $subbrainTypesReverse[$relationName]);
            $relation = $this->subbrain->$relationName;

            $relationExists = $relationContainer->addCheckbox('enabled', 'Enabled')
                ->setOption('render', 'switch')
                ->setOption('hydrateIn', fn(string $name, ?SubbrainRelation $relation): bool => $relation !== null)
                ->setOption('hydrateOut', Hydrator::HYDRATE_OFF);

            $relationExists->addConditionOn($typeInput, Form::EQUAL, $subbrainTypesReverse[$relationName])
                ->setRequired('This relation must be enabled due to subbrain type');

            $relationProperties = $relationMetadata->getProperties();

            foreach ($relationProperties as $propertyName => $relationProperty) {
                switch ($propertyName) {
                    case 'fileName':
                        $relationContainer->addText('fileName', 'File name')
                            ->addConditionOn($relationExists, Form::EQUAL, true)
                            ->setRequired();

                        if ($relation !== null) {
                            $fileDataInputOptions = [
                                'mode' => 'xml',
                            ];
                            $fileDataInput = $this->createFormInput('codeEditor', 'File data', $fileDataInputOptions)
                                ->setOption('hydrateIn',
                                    fn(): string => $this->readRelationFileData(fs: 'data', relation: $relation))
                                ->setOption('hydrateOut', Hydrator::HYDRATE_OFF);
                            $fileDataRefInput = $this->createFormInput('codeEditor', 'File data reference', $fileDataInputOptions)
                                ->setDisabled()
                                ->setOption('hydrateIn',
                                    fn(): string => $this->readRelationFileData(fs: 'dataref', relation: $relation))
                                ->setOption('hydrateOut', Hydrator::HYDRATE_OFF);

                            $relationContainer->addComponent(
                                component: $fileDataInput, name: 'fileData'
                            );
                            $relationContainer->addCheckbox('updateFileData', 'Update file data ?')                                
                                ->setOption('hydrate', Hydrator::HYDRATE_OFF);

                            $relationContainer->addComponent(
                                component: $fileDataRefInput, name: 'fileDataRef'
                            );
                        }
                        break;
                    default:
                        if (str_starts_with($propertyName, 'on')) {
                            $relationContainer->addText($propertyName, $propertyName);
                        }
                        break;
                }
            }
        }

        $this->setupFormButtons($form);

        if ($this->subbrain->isPersisted()) {
            Hydrator::hydrateIn($form, $this->subbrain, Hydrator::DEPTH_UNTIL_EMPTY);
        }

        $form->onSuccess[] = function (Form $form) {
            Hydrator::hydrateOut($form, $this->subbrain);
            foreach ($this->subbrain->getRelationMetadata() as $relationName => $relationMetadata) {
                $relationContainer = $form[$relationName];
                $relationExistsValue = $relationContainer['enabled']->value;

                if (!isset($this->subbrain->$relationName) && $relationExistsValue) {
                    // Relation does NOT EXIST, ADDED in form...
                    $this->subbrain->$relationName = $this->model->getRepositoryForEntity($relationMetadata->className)->create();
                } elseif (isset($this->subbrain->$relationName) && !$relationExistsValue) {
                    // Relation EXISTS, REMOVED in form...
                    $this->model->remove($this->subbrain->$relationName);
                }

                // Hydrate relation
                $relation = $this->subbrain->$relationName;
                if ($relation !== null) {
                    Hydrator::hydrateOut($relationContainer, $relation);

                    // Update file data for given relation
                    if ($relationContainer['updateFileData']?->getValue()) {
                        $this->updateRelationFileData(
                            fs: 'data', relation: $relation, data: $relationContainer['fileData']->getValue(),
                        );
                    }
                }
            }

            $this->formSave($form);
        };

        return $form;
    }

    protected function createComponentGrid($name): DataGrid
    {
        $grid = $this->createGrid();
        $grid->setDataSource($this->repository->findAll());

        $grid->addColumnText('rowActions', 'ID', 'edit');
        $grid->addColumnText('subbrainName', 'Name')
            ->setSortable();

        $grid->addColumnText('subbrainType', 'Type')
            ->setRenderer(fn(Subbrain $subbrain) => $subbrain->subbrainType?->subbrainTypeName);
        
        $grid->addFilterText('subbrainName', 'Name');        
        $grid->addFilterSelect('subbrainType', 'Type', $this->model->subbrainType->getIdNamePairs())
            ->setPrompt('-- no type --');

        return $grid;
    }
}