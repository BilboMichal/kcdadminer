<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use App\Forms\FormFactory;
use App\Grids\GridFactory;
use App\ModConfig;
use App\Model\Entity\Entity;
use App\Model\MapTables;
use App\Model\Model;
use App\Model\Repository;
use League\Flysystem\MountManager;
use Nette\Application\BadRequestException;
use Nette\Application\Helpers;
use Nette\Application\UI\Presenter;
use Nette\Forms\Controls\BaseControl;
use Nette\Forms\ISubmitterControl;
use Nette\Http\SessionSection;
use Nextras\Dbal\Drivers\Exception\ConstraintViolationException;
use Nextras\Dbal\IConnection;
use Ublaboo\DataGrid\DataGrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
abstract class BasePresenter extends Presenter
{
    const FLASH_INFO = 'info';
    const FLASH_SUCCESS = 'success';
    const FLASH_WARNING = 'warning';
    const FLASH_ERROR = 'error';

    private readonly FormFactory $formFactory;
    private readonly GridFactory $gridFactory;
    protected readonly MapTables $mapTables;
    protected readonly ModConfig $modConfig;
    protected readonly Model $model;
    protected readonly IConnection $connection;
    protected readonly MountManager $mountManager;
    public string $appName;
    //
    protected Repository $repository;

    /** @internal */
    protected Entity $entity;
    protected array $breadcrumbs = [];    

    // -------------------------------------------------------------------
    // get/set

    public function inject(
        FormFactory $formFactory,
        GridFactory $gridFactory,
        ModConfig $modConfig,
        MapTables $mapTables, 
        Model $model,
        IConnection $connection,
        MountManager $mountManager,
    )
    {
        $this->formFactory = $formFactory;
        $this->gridFactory = $gridFactory;
        $this->modConfig = $modConfig;
        $this->mapTables = $mapTables;
        $this->model = $model;
        $this->connection = $connection;
        $this->mountManager = $mountManager;

        $this->setupRepository();
    }

    /**
     * Override and setup main repository for presenter
     */
    protected function setupRepository()
    {
        
    }

    /**
     * Settings session
     *
     * @return SessionSection
     */
    final public function getSettingsSection(): SessionSection
    {
        return $this->session->getSection('settings');
    }

    /**
     * Presenter name without modules
     *
     * @return string
     */
    final public function getShortName(): string
    {
        [, $shortName] = Helpers::splitName($this->getName());
        return $shortName;
    }

    /**
     * Presenter global templates directory
     *
     * @return string
     */
    public function getTemplatesDir(): string
    {
        return dirname(static::getReflection()->getFileName()) . '/templates';
    }

    /**
     * Presenter single template directory
     *
     * @return string
     */
    public function getTemplateDir(): string
    {
        return $this->getTemplatesDir() . '/' . $this->getShortName();
    }

    // --------------------------------------------------------------------
    // messages

    public function messageRemoveEntity($id): string
    {
        return sprintf('%s id "%s" deleted', $this->getShortName(), $id);
    }

    public function messageNotFound($id): string
    {
        return sprintf('%s with ID "%s" not found', $this->getShortName(), $id);
    }

    public function messageSaveSuccess($id): string
    {
        return sprintf('%s id "%s" successfully saved', $this->getShortName(), $id);
    }

    public function messageConstraintViolation(ConstraintViolationException $ex): string
    {
        return sprintf('Database constraint violation %s - %s', $ex->getErrorSqlState(), $ex->getMessage());
    }

    // -------------------------------------------------------------------
    // common

    final protected function addBreadcrumb(string $name, string $target = 'this')
    {
        $this->breadcrumbs[] = (object) [
                'name' => $name,
                'target' => $target,
        ];

        return $this;
    }

    final protected function retrieveEntity(string $id = null): Entity
    {
        if (!isset($this->entity)) {
            if ($id !== null) {
                $entity = $this->repository->find($id);
                if ($entity !== null) {
                    $this->entity = $entity;
                } else {
                    throw new BadRequestException($this->messageNotFound($id));
                }
            } else {
                $this->entity = $this->repository->create();
            }
        }

        return $this->entity;
    }

    // -------------------------------------------------------------------
    // forms

    final protected function createForm(): Form
    {
        $form = $this->formFactory->create();
        $form->setHtmlAttribute('data-leave-confirm', true);

        // Form errors --> presenter error flash messages
        $form->onError[] = function (Form $form) {
            foreach ($form->getErrors() as $formError) {
                $this->flashMessage($formError, self::FLASH_ERROR);
            }
        };

        return $form;
    }

    final protected function createFormInput(string $name, ...$args): BaseControl
    {
        return $this->formFactory->createInput($name, ...$args);
    }

    final protected function setupFormButtons(Form $form): self
    {
        $form->addSubmit('save', 'Save')
            ->setOption('afterSave',
                function (): void {
                    $this->redirect('this', $this->entity->id);
                });
        $form->addSubmit('saveReturn', 'Save and return')
            ->setOption('afterSave', function (): void {
                $this->redirect('list');
            });

        return $this;
    }

    final protected function formSave(Form $form): bool
    {
        $submit = $form->isSubmitted();

        if ($submit instanceof ISubmitterControl) {
            $beforeSave = $submit->getOption('beforeSave');
            if ($beforeSave !== null) {
                $beforeSave($submit, $form);
            }
        }

        try {
            $this->repository->persist($this->entity);
        } catch (ConstraintViolationException $ex) {
            $this->flashMessage($this->messageConstraintViolation($ex), self::FLASH_ERROR);
            return false;
        }

        $this->repository->flush();
        $this->flashMessage($this->messageSaveSuccess($this->entity), self::FLASH_SUCCESS);

        if ($submit instanceof ISubmitterControl) {
            $afterSave = $submit->getOption('afterSave');
            if ($afterSave !== null) {
                $afterSave($submit, $form);
            }
        }

        $this->redirect('this');
    }

    // -------------------------------------------------------------------
    // grids

    final protected function createGrid(): DataGrid
    {
        $grid = $this->gridFactory->create();
        
        $gridTemplates = array_map(fn(string $templateDir): string => $templateDir . '/_grid.latte', [
            $this->getTemplatesDir(),
            $this->getTemplateDir()
        ]);
        foreach ($gridTemplates as $gridTemplate) {
            if (file_exists($gridTemplate)) {
                $grid->setTemplateFile($gridTemplate);
            }
        }

        return $grid;
    }

    // -------------------------------------------------------------------
    // runtime

    final public function handleChangeLayoutTemplate($template)
    {
        $layoutTemplates = $this->mapTables->LAYOUT_TEMPLATES->toArray();

        if (in_array($template, $layoutTemplates, true)) {
            $this->getSettingsSection()->layoutTemplate = $template;
        } elseif (!$template) {
            unset($this->getSettingsSection()->layoutTemplate);
        }

        $this->redirect('this');
    }

    protected function startup(): void
    {
        parent::startup();

        
    }

    protected function beforeRender(): void
    {
        $this->template->v = 1;

        $this->template->hasEdit = method_exists($this, 'actionEdit');

        $this->template->breadcrumbs = $this->breadcrumbs;

        $this->template->databaseName = $this->connection->getConfig()['database'] ?? null;

        $this->template->mainMenuItems = $this->mapTables->MAIN_MENU_ITEMS;

        $this->template->layoutTemplates = $this->mapTables->LAYOUT_TEMPLATES;
        $this->template->editorThemes = $this->mapTables->EDITOR_THEMES;
        $this->template->layoutTemplateActive = $this->getSettingsSection()->layoutTemplate ?? null;
        $this->template->editorThemeActive = $this->getSettingsSection()->editorTheme ?? null;
        $this->template->title = $this->appName;
    }

    // -------------------------------------------------------------------
    // override

    public function formatTemplateFiles(): array
    {
        $templateFiles = parent::formatTemplateFiles();
        $templatesDir = $this->getTemplatesDir();
        $templateFiles[] = $templatesDir . '/' . $this->view . '.latte';

        return $templateFiles;
    }
}