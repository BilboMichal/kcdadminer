<?php
declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\BadRequestException;
use Nette\Http\IResponse;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Ramsey\Uuid\Uuid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait UuidValidationTrait
{

    public function messageInvalidUuid($uuid)
    {
        return sprintf('Id "%s" is invalid, must be in uuid format', $uuid);
    }

    final protected function checkUuid(string $uuid = null): self
    {
        try {
            if ($uuid !== null) {
                Uuid::fromString($uuid);
            }
        } catch (InvalidUuidStringException $ex) {
            throw new BadRequestException($this->messageInvalidUuid($uuid), IResponse::S400_BAD_REQUEST);
        }

        return $this;
    }
}