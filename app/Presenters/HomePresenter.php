<?php
declare(strict_types=1);

namespace App\Presenters;

use App\ModConfig;
use Ublaboo\DataGrid\DataGrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class HomePresenter extends BasePresenter
{

    public function actionList()
    {
        $this->template->title = $this->getShortName();
        $this->addBreadcrumb($this->template->title);
    }

    protected function createComponentGrid($name)
    {
        $grid = $this->createGrid();

        $items = [];
        foreach ($this->modConfig as $key => $value) {
            $items[] = (object) [
                'id' => $key,
                'value' => $value,
            ];
        }

        $grid->setDataSource($items);
        $grid->addColumnText('id', 'Key');
        $grid->addColumnText('value', 'Value');

        return $grid;
    }
}