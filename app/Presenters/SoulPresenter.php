<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use App\Forms\Hydrator;
use App\Model\Entity\Soul;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use Ublaboo\DataGrid\DataGrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SoulPresenter extends BasePresenter
{

    use UuidValidationTrait;
    //
    private Soul $soul;

    protected function setupRepository()
    {
        $this->repository = $this->model->soul;
    }

    public function actionList()
    {
        $this->template->title = $this->getShortName();
        $this->addBreadcrumb($this->template->title);
    }

    public function actionEdit(string $id = null, string $copy = null)
    {
        $this->checkUuid($id);
        $this->soul = $this->retrieveEntity($id);
        if ($copy !== null) {
            
        }

        $this->addBreadcrumb($this->getShortName(), 'list');

        $title = $this->soul->isPersisted() ? $this->soul->name : '-- new --';
        $this->addBreadcrumb($title, 'this');
        $this->template->title = $title;
    }

    public function handleDelete(string $id)
    {
        $entity = $this->retrieveEntity($id);

        $this->repository->removeAndFlush($entity);

        $this->flashMessage($this->messageRemoveEntity($entity), self::FLASH_SUCCESS);
        $this->redirect('this');
    }

    protected function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('name', 'Name')
            ->setRequired();

        $brainInput = $this->createFormInput('brain', 'Brain');
        $form->addComponent($brainInput, 'brain');

        $classInput = $this->createFormInput('select2', 'Class', $this->model->soulClass->getIdNamePairs());
        $classInput->setPrompt('-- no class --');
        $form->addComponent($classInput, 'soulClass');

        $form->addText('characterProportion', 'Character proportion')
            ->setHtmlAttribute('type', 'number')
            ->setRequired();
        $form->addText('characterScale', 'Character scale')
            ->setHtmlAttribute('type', 'number')
            ->setHtmlAttribute('step', '0.1')
            ->setRequired();

        $this->setupFormButtons($form);

        if ($this->soul->isPersisted()) {
            Hydrator::hydrateIn($form, $this->soul);
        }

        $form->onSuccess[] = function (Form $form) {
            Hydrator::hydrateOut($form, $this->soul);
            $this->formSave($form);
        };

        return $form;
    }

    protected function createComponentGrid($name): DataGrid
    {
        $grid = $this->createGrid();
        $grid->setDataSource($this->repository->findAll());

        

        /*$grid->setDataSource(new RepositoryDataSource(
            repository: $this->repository,
            condsBuilder: function (array $conds): array {
                if (isset($conds['soulName'])) {
                    
                }
                
                return $conds;
            },
        ));*/

        $grid->addColumnText('rowActions', 'ID', 'edit');
        $grid->addColumnText('soulName', 'Name')
            ->setSortable();

        $grid->addColumnText('brain', 'Brain')
            ->setSortable()
            ->setRenderer(fn(Soul $soul) => $soul->brain->name);
//        $grid->addColumnLink('brainName', 'Brain name', 'Brain:edit', 'brain->brainName');
        $grid->addColumnText('soulClass', 'Class')
            ->setRenderer(fn(Soul $soul) => $soul->soulClass?->name);

//        $grid->addFilterText('id', 'ID');
        $grid->addFilterText('soulName', 'Name');
        $grid->addFilterSelect('brain', 'Brain', $this->model->brain->getIdNamePairs())
            ->setPrompt('-- no brain --');
        
        $grid->addFilterSelect('soulClass', 'Class', $this->model->soulClass->getIdNamePairs())
            ->setPrompt('-- no class --');

        return $grid;
    }
}