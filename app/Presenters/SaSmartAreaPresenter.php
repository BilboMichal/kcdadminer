<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use App\Forms\Hydrator;
use App\Model\Entity\SaSmartArea;
use Ublaboo\DataGrid\DataGrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SaSmartAreaPresenter extends BasePresenter
{

    use UuidValidationTrait;
    //
    private SaSmartArea $saSmartArea;

    protected function setupRepository()
    {
        $this->repository = $this->model->saSmartArea;
    }

    public function actionList()
    {
        $this->template->title = $this->getShortName();
        $this->addBreadcrumb($this->template->title);
    }

    public function actionEdit(string $id = null, string $copy = null)
    {
        $this->checkUuid($id);
        $this->saSmartArea = $this->retrieveEntity($id);

        $this->addBreadcrumb($this->getShortName(), 'list');

        $title = $this->saSmartArea->isPersisted() ? $this->saSmartArea->name : '-- new --';
        $this->addBreadcrumb($title, 'this');
        $this->template->title = $title;
    }

    public function handleDelete(string $id)
    {
        $entity = $this->retrieveEntity($id);

        $this->repository->removeAndFlush($entity);

        $this->flashMessage($this->messageRemoveEntity($entity), self::FLASH_SUCCESS);
        $this->redirect('this');
    }

    protected function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('name', 'Name')
            ->setRequired();

        $brainInput = $this->createFormInput('brain', 'Brain');
        $form->addComponent($brainInput, 'brain');

        $this->setupFormButtons($form);

        if ($this->saSmartArea->isPersisted()) {
            Hydrator::hydrateIn($form, $this->saSmartArea);
        }

        $form->onSuccess[] = function (Form $form) {
            Hydrator::hydrateOut($form, $this->saSmartArea);
            $this->formSave($form);
        };

        return $form;
    }

    protected function createComponentGrid($name): DataGrid
    {
        $grid = $this->createGrid();
        $grid->setDataSource($this->repository->findAll());

        $grid->addColumnText('rowActions', 'ID', 'edit');
        $grid->addColumnText('saSmartAreaName', 'Name')
            ->setSortable();

        $grid->addColumnText('brain', 'Brain')
            ->setSortable()
            ->setRenderer(fn(SaSmartArea $saSmartArea) => $saSmartArea->brain?->name);

        $grid->addFilterText('saSmartAreaName', 'Name');
        $grid->addFilterSelect('brain', 'Brain', $this->model->brain->getIdNamePairs())
            ->setPrompt('-- no brain --');

        return $grid;
    }
}