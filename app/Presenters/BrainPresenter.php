<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\Form;
use App\Forms\Hydrator;
use App\Model\Entity\Brain;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use Ublaboo\DataGrid\DataGrid;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class BrainPresenter extends BasePresenter
{

    use UuidValidationTrait;
    //
    private Brain $brain;

    protected function setupRepository()
    {
        $this->repository = $this->model->brain;
    }

    public function actionList()
    {
        $this->template->title = $this->getShortName();
        $this->addBreadcrumb($this->template->title);
    }

    public function actionEdit(string $id = null)
    {
        $this->checkUuid($id);
        $this->brain = $this->retrieveEntity($id);

        $this->addBreadcrumb($this->getShortName(), 'list');

        $title = $this->brain->isPersisted() ? $this->brain->brainName : '-- new --';
        $this->addBreadcrumb($title, 'this');
        $this->template->title = $title;
    }

    public function handleDelete(string $id)
    {
        $entity = $this->retrieveEntity($id);

        $this->repository->removeAndFlush($entity);

        $this->flashMessage($this->messageRemoveEntity($entity), self::FLASH_SUCCESS);
        $this->redirect('this');
    }

    protected function createComponentForm(): Form
    {
        $form = $this->createForm();

        $form->addText('brainName', 'Name')
            ->setRequired();
        $form->addComponent(
            component: $this->createFormInput('subbrains', 'Subbrains')->setOption('hydrateIn', 'subbrainsIds'),
            name: 'subbrains'
        );

        $this->setupFormButtons($form);

        if ($this->brain->isPersisted()) {
            Hydrator::hydrateIn($form, $this->brain);
        }

        $form->onSuccess[] = function (Form $form) {
            Hydrator::hydrateOut($form, $this->brain);
            $this->formSave($form);
        };

        return $form;
    }

    protected function createComponentGrid($name): DataGrid
    {
        $grid = $this->createGrid();
        $grid->setDataSource($this->repository->findAll());

        $grid->addColumnText('rowActions', 'ID', 'edit');
        $grid->addColumnText('brainName', 'Name')
            ->setSortable();

        $grid->addFilterText('brainName', 'Name');

        return $grid;
    }
}