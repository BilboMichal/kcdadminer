<?php
declare(strict_types=1);

namespace App;

use Tracy\ILogger;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class ErrorLogger implements ILogger
{
    private array $handlers = [];

    public function addHandler(callable $handler)
    {
        $this->handlers[] = $handler;
    }

    public function log(mixed $value, string $level = self::INFO)
    {
        foreach ($this->handlers as $handler) {
            $handler($value, $level);
        }
    }
}