<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\InvalidStateException;
use Nextras\Orm\Mapper\Dbal\Conventions\Conventions;
use Nextras\Orm\Mapper\Dbal\Conventions\IConventions;
use Bilbofox\Nextras\Orm\Mapper as NextrasOrmMapper;
use Nextras\Orm\StorageReflection\StringHelper;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Mapper extends NextrasOrmMapper
{

    protected function createConventions(): IConventions
    {
        $conventions = parent::createConventions();
        if ($conventions instanceof Conventions) {
            // table2another
            $conventions->manyHasManyStorageNamePattern = '%s2%s';
        } else {
            throw new InvalidStateException(sprintf('$conventions must be "%s" instance', Conventions::class));
        }

        return $conventions;
    }
}