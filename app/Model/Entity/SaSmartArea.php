<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Entity\Entity;
use App\Model\Property\UuidProperty;
use Ramsey\Uuid\UuidInterface;

/**
 * @property string $id {primary-proxy}
 * @property UuidInterface $saSmartAreaId {primary} {wrapper UuidProperty}
 *
 * @property string $saSmartAreaName
 * @property string $name {virtual}
 *
 * @property Brain|null $brain {m:1 Brain::$saSmartAreas}
 *
 * @see UuidProperty
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SaSmartArea extends Entity
{
    
}