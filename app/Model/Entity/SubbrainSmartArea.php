<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property Subbrain $subbrainId {1:1 Subbrain::$smartArea, isMain=true} {primary}
 *
 * @property string $fileName
 * @property string $filePath {virtual}
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SubbrainSmartArea extends SubbrainRelation
{

}