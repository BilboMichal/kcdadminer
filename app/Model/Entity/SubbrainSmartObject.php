<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property Subbrain $subbrainId  {1:1 Subbrain::$smartObject, isMain=true} {primary}
 *
 * @property string $fileName
 * @property string $filePath {virtual}
 *
 * @property string|null $onRequestTree
 * @property string|null $onReleaseTree
 * @property string|null $onUpdateTree
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SubbrainSmartObject extends SubbrainRelation
{

}