<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Property\UuidProperty;
use Ramsey\Uuid\UuidInterface;

/**
 * @property string $id {primary-proxy}
 * @property UuidInterface $brainId {primary} {wrapper UuidProperty}
 *
 * @property string $brainName
 * @property string $name {virtual}
 *
 * @property Soul[] $souls {1:m Soul::$brain}
 * @property SaSmartArea[] $saSmartAreas {1:m SaSmartArea::$brain}
 *
 * @property Subbrain[] $subbrains {m:m Subbrain::$brains, isMain=true}
 * @property-read array $subbrainsIds {virtual}
 * 
 * @see UuidProperty
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Brain extends Entity
{

    protected function getterSubbrainsIds(): array
    {
        return self::entitiesIds($this->subbrains);
    }
}