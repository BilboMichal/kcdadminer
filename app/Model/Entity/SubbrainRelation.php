<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Orm\Entity\Reflection\PropertyMetadata;

/**
 * @property string $id {primary-proxy}
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
abstract class SubbrainRelation extends Entity
{

    protected function getterFilePath(): string
    {
        return 'Libs/ai/'.$this->fileName;
    }
}