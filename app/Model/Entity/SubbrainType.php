<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property int $id {primary-proxy}
 * @property int $subbrainTypeId {primary}
 * 
 * @property string $subbrainTypeName
 * @property string $name {virtual}
 *
 * @property Subbrain[] $subbrains {1:m Subbrain::$subbrainType}
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class SubbrainType extends Entity
{
    
}