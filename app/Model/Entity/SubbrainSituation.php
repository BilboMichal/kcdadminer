<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property Subbrain $subbrainId {1:1 Subbrain::$situation, isMain=true} {primary}
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SubbrainSituation extends SubbrainRelation
{

}