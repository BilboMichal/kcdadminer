<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property Subbrain $subbrainId {1:1 Subbrain::$combat, isMain=true} {primary}
 *
 * @property string $fileName
 * @property string $filePath {virtual}
 *
 * @property string $treeName
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SubbrainCombat extends SubbrainRelation
{

}