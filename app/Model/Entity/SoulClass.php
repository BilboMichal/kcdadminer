<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @property int $id {primary-proxy}
 * @property int $soulClassId {primary}
 *
 * @property string $soulClassName
 * @property string $name {virtual}
 *
 * @property Soul[] $souls {1:m Soul::$soulClass}
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SoulClass extends Entity
{
    
}