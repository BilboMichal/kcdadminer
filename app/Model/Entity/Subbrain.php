<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Property\UuidProperty;
use Nextras\Orm\Entity\Reflection\EntityMetadata;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;
use Ramsey\Uuid\UuidInterface;

/**
 * @property            string                         $id                 {primary-proxy}
 * @property            UuidInterface                  $subbrainId         {primary} {wrapper UuidProperty}
 *
 * @property            string                         $subbrainName
 * @property            string                         $name               {virtual}
 * @property            bool                           $alwaysActive       {default false}
 *
 * @property            SubbrainType|null              $subbrainType       {m:1 SubbrainType::$subbrains}
 *
 * @property-read       string|null                    $relationType       {virtual}
 * @property-read       SubbrainRelation|null          $relation           {virtual}
 * @property-read       SubbrainRelation[]             $relations          {virtual}
 * @property-read       SubbrainRelation[]             $existingRelations  {virtual}
 *
 * @property            SubbrainBehaviourTree|null     $behaviorTree       {1:1 SubbrainBehaviourTree::$subbrainId}
 * @property            SubbrainSmartArea|null         $smartArea          {1:1 SubbrainSmartArea::$subbrainId}
 * @property            SubbrainSmartObject|null       $smartObject        {1:1 SubbrainSmartObject::$subbrainId}
 * @property            SubbrainCombat|null            $combat             {1:1 SubbrainCombat::$subbrainId}
 * @property            SubbrainDialog|null            $dialog             {1:1 SubbrainDialog::$subbrainId}
 * @property            SubbrainSwitching|null         $switching          {1:1 SubbrainSwitching::$subbrainId}
 * @property            SubbrainSituation|null         $situation          {1:1 SubbrainSituation::$subbrainId}
 *
 * @property            Brain[]                        $brains             {m:m Brain::$subbrains}
 * @property-read       int[]                          $brainsIds          {virtual}
 *
 * @see UuidProperty
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Subbrain extends Entity
{

    /**
     *
     * @return PropertyMetadata[]
     */
    public function getRelationProperties(): array
    {
        return array_filter(
            $this->getMetadata()->getProperties(),
            fn(PropertyMetadata $property): bool => isset($property->relationship) &&
            $property->relationship->property === 'subbrainId'
        );
    }

    /**
     *
     * @return EntityMetadata[]
     */
    public function getRelationMetadata(): array
    {
        return array_map(
            fn(PropertyMetadata $property): EntityMetadata => $property->relationship->entityMetadata,
            $this->getRelationProperties()
        );
    }

    /**
     *
     * @return SubbrainRelation[]
     */
    protected function getterRelations(): array
    {
        return array_map(
            fn(PropertyMetadata $property): ?SubbrainRelation => $this->getValue($property->name),
            $this->getRelationProperties()
        );
    }

    /**
     *
     * @return SubbrainRelation[]
     */
    protected function getterExistingRelations(): array
    {
        return array_filter($this->getterRelations());
    }

    protected function getterRelationType(): ?string
    {
        $typeName = $this->subbrainType?->subbrainTypeName;
        return $typeName !== null ? lcfirst($typeName) : null;
    }

    protected function getterRelation(): ?SubbrainRelation
    {
        return $this->{$this->relationType};
    }

    protected function getterBrainsIds(): array
    {
        return self::entitiesIds($this->brains);
    }
}