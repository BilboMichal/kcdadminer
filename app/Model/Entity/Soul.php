<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Property\UuidProperty;
use Ramsey\Uuid\UuidInterface;

/**
 * @property string $id {primary-proxy}
 * @property UuidInterface $soulId {primary} {wrapper UuidProperty}
 *
 * @property string $soulName
 * @property string $name {virtual}
 *
 * @property Brain|null $brain {m:1 Brain::$souls}
 *
 * @property SoulClass|null $soulClass {m:1 SoulClass::$souls}
 *
 * @property int $characterProportion
 * @property float $characterScale
 *
 * @see UuidProperty
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Soul extends Entity
{
    
}