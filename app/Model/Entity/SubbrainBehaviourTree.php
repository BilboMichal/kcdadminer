<?php

namespace App\Model\Entity;

use League\Flysystem\FilesystemInterface;
use League\Flysystem\MountManager;

/**
 * @property Subbrain $subbrainId {1:1 Subbrain::$behaviorTree, isMain=true} {primary}
 *
 * @property string $fileName
 * @property string $filePath {virtual}
 *
 * @property string $treeName
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class SubbrainBehaviourTree extends SubbrainRelation
{

}