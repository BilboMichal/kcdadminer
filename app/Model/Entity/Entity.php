<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Orm\Entity\Entity as NextrasOrmEntity;
use Nextras\Orm\Relationships\IRelationshipCollection;

/**
 * Base of all application entities
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
abstract class Entity extends NextrasOrmEntity
{

    protected function getterName(): string
    {
        return $this->{$this->getEntityName() . 'Name'};
    }

    protected function setterName(string $value)
    {
        $this->{$this->getEntityName() . 'Name'} = $value;
    }

    final public function addData(iterable $data): self
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $this->$key->setData($value);
            } else {
                $this->$key = $value;
            }
        }

        return $this;
    }

    /**
     * App\Model\Entity\MyEntity --> myEntity
     *
     * @return string
     */
    final public function getEntityName(): string
    {
        $className = $this->getMetadata()->className;
        return lcfirst(substr($className, strrpos($className, '\\') + 1));
    }

    /**
     * entity[] -> id[]
     *
     * @param iterable $entities
     * @return array
     */
    final protected static function entitiesIds(iterable $entities): array
    {
        return array_map(
            fn(Entity $entity) => $entity->id, is_array($entities) ? $entities : iterator_to_array($entities)
        );
    }

    /**
     * Behaviour changed from parent entity --> so it can delegate into relationship setter
     */
    public function __set($name, $value): void
    {
        if ($this->hasValue($name)) {
            $property = $this->getValue($name);
        }
        if (isset($property) && $property instanceof IRelationshipCollection) {
            $property->set($value);
        } else {
            $this->setValue($name, $value);
        }
    }

    /**
     * entity -> ID
     *
     * @return string
     */
    public function __toString(): string
    {
        return strval($this->id ?? '');
    }
}