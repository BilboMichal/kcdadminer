<?php
declare(strict_types=1);

namespace App\Model\Property;

use Nextras\Orm\Entity\ImmutableValuePropertyWrapper;
use Ramsey\Uuid\Uuid;

/**
 * Wrapper for ID in UUID format
 *
 * Generated new UUID for empty value
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class UuidProperty extends ImmutableValuePropertyWrapper
{

    public function convertToRawValue($value)
    {
        return (string) $value;
    }

    public function convertFromRawValue($value)
    {
        return isset($value) ? Uuid::fromString($value) : Uuid::uuid4();
    }
}