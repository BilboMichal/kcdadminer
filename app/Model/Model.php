<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Model\Model as NextrasOrmModel;

/**
 * @property-read Repository $soul
 * @property-read Repository $soulClass
 * @property-read Repository $brain
 * @property-read Repository $subbrain
 * @property-read Repository $subbrainType
 * @property-read Repository $saSmartArea
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Model extends NextrasOrmModel
{
    
}