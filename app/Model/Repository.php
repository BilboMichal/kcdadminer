<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\Entity;
use Bilbofox\Nextras\Orm\Repository as NextrasOrmRepository;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Repository extends NextrasOrmRepository
{

    /**
     * App\Model\Entity\MyEntity --> myEntity
     *
     * @return string
     */
    final public function getEntityName(): string
    {
        $className = $this->getEntityMetadata()->className;
        return lcfirst(substr($className, strrpos($className, '\\') + 1));
    }

    /**
     * property --> myEntityProperty
     *
     * @param string $name
     * @return string
     */
    final public function getPrefixedName(string $name): string
    {
        return $this->getEntityName() . ucfirst($name);
    }

    public function create(array $data = []): Entity
    {
        /* @var $entity Entity */
        $entity = new ($this->getEntityClassName($data));
        $this->attach($entity);
        $entity->addData($data);

        return $entity;
    }

    public function find(mixed $id): ?Entity
    {
        return $this->findById($id)->fetch();
    }

    public function getPairs(string $key = null, string $value = null): array
    {
        return $this->findAll()->fetchPairs($key, $value);
    }

    public function getIdPairs(string $value = null): array
    {
        return $this->getPairs(key: 'id', value: $value);
    }

    public function getIdNamePairs(): array
    {
        return $this->getPairs(key: 'id', value: 'name');
    }
}