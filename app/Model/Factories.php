<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Repository\Repository;
use Nextras\Orm\Mapper\IMapper;
use Nextras\Orm\Repository\IDependencyProvider;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Factories
{

    public static function createRepository(string $entityClass, IMapper $mapper, IDependencyProvider $dependencyProvider): Repository
    {
        $repository = new Repository($mapper, $dependencyProvider);
        $repository->setEntityClassName($entityClass);

        return $repository;
    }
}