<?php
declare(strict_types=1);

namespace App\Model;

use Nette\Utils\ArrayHash;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class MapTables extends ArrayHash
{

    public function toArray(): array
    {
        return (array) $this;
    }
}