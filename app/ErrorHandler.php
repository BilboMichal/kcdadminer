<?php
declare(strict_types=1);

namespace App;

use ErrorException;
use Throwable;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class ErrorHandler
{
    private static array $handlers = [];

    public static function install()
    {
        error_reporting(E_ALL);

        // PHP8 error handler
        set_error_handler(function (int $errno, string $message, string $file, int $line) {
            if (!(error_reporting() & $errno)) {
                // This error code is not included in error_reporting, so let it fall
                // through to the standard PHP error handler
                return false;
            }

            // All errors --> into ErrorException
            throw new ErrorException($message, 0, $errno, $file, $line);
        });

        set_exception_handler(__CLASS__ . '::handleException');
    }

    public static function handleException(...$args)
    {
        foreach (self::$handlers as $handler) {
            $handler(...$args);
        }
    }

    public static function addHandler(callable $handler)
    {
        self::$handlers[] = $handler;
    }
}