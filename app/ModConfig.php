<?php
declare(strict_types=1);

namespace App;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Loading configuration files from game
 * Config files are basically INI files with comments as -- instead of ;
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class ModConfig implements IteratorAggregate, ArrayAccess
{
    private array $config = [];

    // -------------------------------------------------------
    // factories

    public function __construct(array $config = [])
    {
        $this->setConfig($config);
    }

    public static function fromFiles(array $configFiles): self
    {
        return (new self)->addConfigFiles($configFiles);
    }

    // -------------------------------------------------------
    // helpers

    private static function parseConfigFile(string $configFile): array
    {
        return parse_ini_string(
            ini_string: str_replace('--', ';', file_get_contents($configFile)),
            process_sections: false,
            scanner_mode: INI_SCANNER_TYPED
        );
    }

    // -------------------------------------------------------
    // config

    public function addConfigFiles(array $configFiles)
    {
        foreach ($configFiles as $configFile) {
            $this->addConfig(self::parseConfigFile($configFile));
        }

        return $this;
    }

    public function addConfig(array $config)
    {
        $this->config = $config + $this->config;
        return $this;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    public function toArray(): array
    {
        return $this->config;
    }

    // -------------------------------------------------------
    // ~ArrayAccess

    public function offsetExists(mixed $key): bool
    {
        return isset($this->config[$key]);
    }

    public function offsetGet(mixed $key): mixed
    {
        return $this->config[$key];
    }

    public function offsetSet(mixed $key, mixed $value): void
    {
        $this->config[$key] = $value;
    }

    public function offsetUnset(mixed $key): void
    {
        unset($this->config[$key]);
    }

    // -------------------------------------------------------
    // ~IteratorAggregate

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->config);
    }
}