<?php
declare(strict_types=1);

namespace App\Helpers;

/**
 * XML script allows write codes inside attributes in better human readable way:
 *
 * <element code="{var foo='bar'; var result = 5 + 2; this.callMethod();}"></element>
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class XmlScript
{
    /**
     * From raw XML to script XML
     *
     * @param string $xml
     * @return string
     */
    public static function decode(string $xml): string
    {
        return preg_replace_callback('/="([^"]*?)"/', function (array $m): string {
            $xmlAttr = $m[1];
            $wrapLeft = '';
            $wrapRight = '';
            if (str_starts_with($xmlAttr, '&quot;') && str_ends_with($xmlAttr, '&quot;')) {
                $wrapLeft = '{';
                $wrapRight = '}';
                $xmlAttr = trim($xmlAttr, '&quot;');
            }

            return '="' . $wrapLeft . str_replace('"', "'", html_entity_decode($xmlAttr, ENT_QUOTES | ENT_XML1)) . $wrapRight . '"';
        }, $xml);
    }

    /**
     * From script XML to raw XML
     *
     * @param string $scriptXml
     * @return string
     */
    public static function encode(string $scriptXml): string
    {
        return preg_replace_callback('/="([^"]*?)"/', function (array $m): string {
            $xmlAttr = $m[1];
            $wrapLeft = '';
            $wrapRight = '';
            if (str_starts_with($xmlAttr, '{') && str_ends_with($xmlAttr, '}')) {
                $wrapLeft = '&quot;';
                $wrapRight = '&quot;';
                $xmlAttr = rtrim(ltrim($xmlAttr, '{'), '}');
            }

            return '="' . $wrapLeft . htmlentities($xmlAttr, ENT_QUOTES | ENT_XML1) . $wrapRight . '"';
        }, $scriptXml);
    }
}