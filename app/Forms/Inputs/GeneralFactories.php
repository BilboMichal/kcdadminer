<?php
declare(strict_types=1);

namespace App\Forms\Inputs;

use App\Forms\Controls\AttributeDecorator;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\SelectBox;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class GeneralFactories
{

    public static function select2(string $label = null, array $items = [], array $options = []): SelectBox
    {
        $control = new SelectBox($label, $items);
        $control->setPrompt('-- Select item --');
        $control->setHtmlAttribute('data-placeholder', $control->getPrompt());
        AttributeDecorator::setControlAttribute($control, 'select2', $options + [
            'allowClear' => true,
        ]);

        return $control;
    }

    public static function select2Multi(string $label = null, array $items = [], array $options = []): MultiSelectBox
    {
        $control = new MultiSelectBox($label, $items);
        AttributeDecorator::setControlAttribute($control, 'select2', $options);

        return $control;
    }
}