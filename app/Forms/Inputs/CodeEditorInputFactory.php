<?php
declare(strict_types=1);

namespace App\Forms\Inputs;

use App\Forms\Controls\AttributeDecorator;
use App\Forms\Controls\TextArea;
use App\Forms\Validators;
use App\Helpers\XmlScript;
use Nette\Http\Session;
use Nette\Http\SessionSection;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class CodeEditorInputFactory
{
    const DEFAULT_OPTIONS = [
        
    ];

    private SessionSection $settingsSection;

    public function __construct(Session $session, private array $options = [])
    {
        $this->settingsSection = $session->getSection('settings');
    }

    public function create(string $label = null, array $options = []): TextArea
    {
        $control = new TextArea($label);
        $options = self::DEFAULT_OPTIONS + $this->options + $options;

        $mode = $options['mode'] ?? null;

                /*if (isset($this->settingsSection->editorTheme) && !isset($options['theme'])) {
            $options['theme'] = 'ace/theme/' . $this->settingsSection->editorTheme;
        }*/        

        $control = AttributeDecorator::setControlAttribute($control, 'code-editor', array_filter($options));
        if ($mode === 'xml') {
            $control->setOption('valueIn', fn(?string $value): ?string => $value !== null ? XmlScript::decode($value) : null)
                ->setOption('valueOut', fn(?string $value): ?string => $value !== null ? trim(XmlScript::encode($value)) : null);
        }

        return $control;
    }

    public function __invoke()
    {
        return $this->create(...func_get_args());
    }
}