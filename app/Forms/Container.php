<?php
declare(strict_types=1);

namespace App\Forms;

use Nette\Forms\Container as NetteContainer;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Container extends NetteContainer
{
    use ContainerExtendTrait;
}