<?php
declare(strict_types=1);

namespace App\Forms;

use Nette\Forms\Controls\BaseControl;
use Nette\InvalidStateException;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class FormInputFactory
{
    private array $factories = [];

    public function __construct(array $factories = [])
    {
        foreach ($factories as $name => $factory) {
            $this->add($name, $factory);
        }
    }

    /**
     * Add registered input factory
     *
     * @param string $name
     * @param callable $factory
     * @return self
     */
    public function add(string $name, callable $factory): self
    {
        $this->factories[$name] = $factory;
        return $this;
    }

    /**
     *
     * @param string $name
     * @param mixed $args
     * @return BaseControl
     * @throws InvalidStateException
     */
    public function create(string $name, ...$args): BaseControl
    {
        if (!isset($this->factories[$name])) {
            throw new InvalidStateException(sprintf('Input factory for "%s" was not registered', $name));
        }

        return $this->factories[$name](...$args);
    }

    public function __invoke()
    {
        return $this->create(...func_get_args());
    }
}