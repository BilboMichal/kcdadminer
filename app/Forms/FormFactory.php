<?php
declare(strict_types=1);

namespace App\Forms;

use App\Presenters\BasePresenter;
use Nette\Forms\Controls\BaseControl;
use Nette\InvalidStateException;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class FormFactory
{
    /** @var callable */
    private $inputFactory;
    private array $options = [
        // Enables AJAX sending forms
        'ajax' => false,
    ];

    public function __construct(array $options = [])
    {
        $this->options = array_intersect_key($options, $this->options) + $this->options;
    }

    public function getInputFactory(): ?callable
    {
        return $this->inputFactory;
    }

    public function setInputFactory(callable $inputFactory)
    {
        $this->inputFactory = $inputFactory;
        return $this;
    }

    /**
     *
     * @return Form
     */
    public function create(): Form
    {
        $form = new Form;
        /* if ($this->options['ajax']) {
          $form->setHtmlAttribute('class', 'ajax');
          } */
//        $form->setRenderer($this->rendererFactory->create());

       

        return $form;
    }

    /**
     *
     * @param string $name
     * @param mixed $args
     * @return BaseControl
     * @throws InvalidStateException
     */
    public function createInput(string $name, ...$args): BaseControl
    {
        if (!isset($this->inputFactory)) {
            throw new InvalidStateException('$inputFactory is not set');
        }

        return ($this->inputFactory)($name, ...$args);
    }

    public function __invoke(): Form
    {
        return $this->create();
    }
}