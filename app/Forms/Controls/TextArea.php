<?php
declare(strict_types=1);

namespace App\Forms\Controls;

use Nette\Forms\Controls\TextArea as NetteTextArea;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class TextArea extends NetteTextArea
{

    use ValueFiltersTrait;
}