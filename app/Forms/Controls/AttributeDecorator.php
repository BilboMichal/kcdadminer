<?php
declare(strict_types=1);

namespace App\Forms\Controls;

use Nette\Forms\Controls\BaseControl;
use Nette\Utils\Json;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class AttributeDecorator
{
    const CONTROL_ATTR = 'data-control';
    const CONTROL_OPTIONS_ATTR = 'data-control-options';

    public static function setControlAttribute(BaseControl $control, $value, array $options = []): BaseControl
    {
        $value = (array) $value;
        if ($value) {
            $control->setHtmlAttribute(self::CONTROL_ATTR, implode('|', $value));
        }
        if ($options) {
            self::setControlOptionsAttribute($control, $options);
        }

        return $control;
    }

    public static function setControlOptionsAttribute(BaseControl $control, array $options): BaseControl
    {
        if ($options) {
            $control->setHtmlAttribute(self::CONTROL_OPTIONS_ATTR, Json::encode($options));
        }
        return $control;
    }
}