<?php
declare(strict_types=1);

namespace App\Forms\Controls;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait ValueFiltersTrait
{

    public function getValue()
    {
        $value = parent::getValue();

        $valueOut = $this->getOption('valueOut');
        if ($valueOut !== null) {
            $value = call_user_func($valueOut, $value);
        }

        return $value;
    }

    public function setValue($value)
    {
        $valueIn = $this->getOption('valueIn');
        if ($valueIn !== null) {
            $value = call_user_func($valueIn, $value);
        }

        return parent::setValue($value);
    }
}