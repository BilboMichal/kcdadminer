<?php
declare(strict_types=1);

namespace App\Forms;

use Nette\Forms\Controls\BaseControl;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Validators
{

    public static function validateXml(BaseControl $input, $arg): bool
    {
        $xmlUseErrors = libxml_use_internal_errors(true);
        $xmlValid = simplexml_load_string($input->getValue());
        libxml_use_internal_errors($xmlUseErrors);
        
        return $xmlValid !== false;
    }
}