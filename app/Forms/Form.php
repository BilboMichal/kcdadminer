<?php
declare(strict_types=1);

namespace App\Forms;

use Nette\Application\UI\Form as NetteForm;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class Form extends NetteForm
{
    use ContainerExtendTrait;
}