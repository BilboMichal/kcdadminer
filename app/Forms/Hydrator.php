<?php

namespace App\Forms;

use Nette\ComponentModel\IComponent;
use Nette\ComponentModel\IContainer;
use Nette\Forms\IControl;
use Nette\Forms\ISubmitterControl;

/**
 * form <=> object, both sides hydrator
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Hydrator
{
    const HYDRATE_OFF = -1;
    //
    const DEPTH_UNLIMITED = -1;
    const DEPTH_UNTIL_EMPTY = -2;
    const DEPTH_ONCE = 1;
    const DEPTH_NONE = 0;

    private static function getControlHydrate(IComponent $control, string $direction): mixed
    {
        $hydrate = $control->getName();
        if (method_exists($control, 'getOptions')) {
            $options = $control->getOptions();
            if (array_key_exists('hydrate' . ucfirst($direction), $options)) {
                $hydrate = $options['hydrate' . ucfirst($direction)];
            } elseif (array_key_exists('hydrate', $options)) {
                $hydrate = $options['hydrate'];
            }
        }

        return $hydrate;
    }

    /**
     * object --> form container
     *
     * @param IContainer $container
     * @param object $data
     * @param int $depth
     * @return void
     */
    public static function hydrateIn(IContainer $container, object $data, int $depth = self::DEPTH_UNLIMITED): void
    {
        if ($depth === 0) {
            return;
        }

        foreach ($container->getComponents() as $control) {
            $name = $control->getName();
            $hydrate = self::getControlHydrate($control, 'in');
            if ($hydrate === self::HYDRATE_OFF) {
                // Skip hydration of this control ^
                continue;
            }

            if ($control instanceof IControl && !$control instanceof ISubmitterControl) {
                $control->setValue(is_callable($hydrate) ? $hydrate($name, $data, $control) : ($data->$hydrate ?? null));
            } elseif ($control instanceof IContainer) {
                if ($depth === self::DEPTH_UNTIL_EMPTY && !isset($data->$hydrate)) {
                    // Reached empty - allowed to skip ^
                    continue;
                }
                // Classic unlimited -|
                //                    |
                //                    v
                self::hydrateIn($control, $data->$hydrate, $depth > 0 ? --$depth : $depth);
            }
        }
    }

    /**
     * form container --> object
     *
     * @param IContainer $container
     * @param object $data
     * @param int $depth
     * @return void
     */
    public static function hydrateOut(IContainer $container, object $data, int $depth = self::DEPTH_UNLIMITED): void
    {
        if ($depth === 0) {
            return;
        }

        foreach ($container->getComponents() as $control) {
            $name = $control->getName();
            $hydrate = self::getControlHydrate($control, 'out');
            if ($hydrate === self::HYDRATE_OFF) {
                // Skip hydration of this control ^
                continue;
            }

            if ($control instanceof IControl && !$control instanceof ISubmitterControl) {
                $value = $control->getValue();
                if (is_callable($hydrate)) {
                    $hydrate($name, $data, $value, $control);
                } else {
                    $data->$hydrate = $value;
                }
            } elseif ($control instanceof IContainer) {
                $hydrateOutData = is_callable($hydrate) ? $hydrate($name, $data, $value, $control) : $data->$hydrate ?? null;
                if ($hydrateOutData === null && $depth === self::DEPTH_UNTIL_EMPTY) {
                    // Reached empty - allowed to skip ^
                    continue;
                }
                // Classic unlimited -|
                //                    |
                //                    v
                self::hydrateOut($control, $hydrateOutData, $depth > 0 ? --$depth : $depth);
            }
        }
    }
}