<?php
declare(strict_types=1);

namespace Config\Latte;

use Bilbofox\Latte\AssetExtension;
use Bilbofox\Latte\Formatters\FileVersionFormatter;
use Bilbofox\Latte\Formatters\TryMinFormatter;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class AssetExtensionFactory
{

    public function __construct(private string $wwwDir, private bool $debug)
    {
        
    }

    public function create(): AssetExtension
    {
        $assetExtension = new AssetExtension;
        /* if (!$this->debug) {
          $assetExtension->addFormatter(new TryMinFormatter($this->cf->wwwDir));
          } */
        $assetExtension->addFormatter(new FileVersionFormatter($this->wwwDir));

        return $assetExtension;
    }
}