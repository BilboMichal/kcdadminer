<?php
declare(strict_types=1);

namespace Config;

use Nette\DI\CompilerExtension;
use Nette\DI\Container;
use Nette\PhpGenerator\ClassType;
use Psr\Log\LoggerInterface;

/**
 * Executes code after DI container is compiled
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class InitExtension extends CompilerExtension
{

    public function __construct(private readonly bool $debugMode)
    {
        
    }

    public static function init(Container $container)
    {
        $logger = $container->getByType(LoggerInterface::class);
    }

    public function afterCompile(ClassType $class): void
    {
        $this->initialization->addBody(__CLASS__ . '::init($this);');
    }
}