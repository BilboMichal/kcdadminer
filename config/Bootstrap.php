<?php
declare(strict_types=1);

namespace Config;

use Dotenv\Dotenv;
use Latte\Bridges\Tracy\BlueScreenPanel as TracyLatteBluescreen;
use Nette\Bootstrap\Configurator;
use Tracy\Bridges\Nette\Bridge as TracyNetteBridge;
use Tracy\Debugger;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Bootstrap
{

    public static function installTracy(bool $debug, string $logDir)
    {
        Debugger::enable($debug ? Debugger::DEVELOPMENT : Debugger::PRODUCTION, $logDir);
        TracyNetteBridge::initialize();
        TracyLatteBluescreen::initialize();

        Debugger::$showLocation = true;
        Debugger::$strictMode = true;
        Debugger::$maxLength = intval($env['TRACY_MAX_LENGTH'] ?? 2000);
        Debugger::$maxDepth = intval($env['TRACY_MAX_DEPTH'] ?? 5);
    }

    /**
     * Boots up global environment and returns Nette configurator
     *
     * @return Configurator
     */
    public static function boot(): Configurator
    {
        // ---------------------------------------------
        // Global

        error_reporting(E_ALL);

        $rootDir = __DIR__ . '/..';
        $appDir = $rootDir . '/app';
        $configDir = $rootDir . '/config';
        $logDir = $rootDir . '/log';
        $wwwDir = $rootDir . '/www';
        $tempDir = $rootDir . '/temp';

        $timezone = 'Europe/Prague';
        $appName = 'KCD Adminer';

        // ---------------------------------------------
        // Env + config

        $env = Dotenv::createImmutable($rootDir)->load();
        $debug = boolval($env['DEBUG'] ?? false);

        // ---------------------------------------------
        // Configurator

        $configurator = new Configurator();
        $configurator->addParameters([
            'debug' => $debug,
            'appDir' => $appDir,
            'logDir' => $logDir,
            'modDir' => $env['MOD_DIR'],
            'sequenceOffset' => $env['SEQUENCE_OFFSET'] ?? null,
            'wwwDir' => $wwwDir,
            'timezone' => $timezone,
        ])->setTempDirectory($tempDir)->setTimeZone($timezone);

        // ---------------------------------------------
        // Debugger

        self::installTracy($debug, $logDir);

        // ---------------------------------------------
        // App config

        $configurator->defaultExtensions['init'] = [InitExtension::class, ['%debugMode%']];

        $configurator->addConfig($configDir . '/config.neon');
        $configurator->addConfig($configDir . '/maptables.neon');
        $localNeon = $configDir . '/local.neon';
        if (file_exists($localNeon)) {
            $configurator->addConfig($localNeon);
        }

        return $configurator;
    }
}