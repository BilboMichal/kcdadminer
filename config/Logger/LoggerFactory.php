<?php
declare(strict_types=1);

namespace Config\Logger;

use Monolog\Logger;
use Psr\Log\LoggerInterface;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class LoggerFactory
{

    public static function create(string $name): LoggerInterface
    {
        $logger = new Logger(name: $name);
        return $logger;
    }

    public static function createSimpleLogger()
    {
        
    }
}