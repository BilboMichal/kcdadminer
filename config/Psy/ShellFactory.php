<?php
declare(strict_types=1);

namespace Config\Psy;

use Nette\DI\Container;
use Psy\Configuration;
use Psy\Shell;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class ShellFactory
{

    public static function create(string $appName, Container $container = null): Shell
    {
        $shConfig = new Configuration([
            'startupMessage' => sprintf('Interactive shell for app %s - welcome...', $appName),
        ]);
        $shOutput = $formatter = $shConfig->getOutput();

        $formatter = $shOutput->getFormatter();
        $formatter->setStyle('aside', new OutputFormatterStyle('cyan'));
        $formatter->setStyle('class', new OutputFormatterStyle('yellow', null, ['underscore']));
        $formatter->setStyle('error', new OutputFormatterStyle('white', 'red', ['bold']));

        $shell = new Shell($shConfig);
        $shell->setScopeVariables(compact('container'));

        return $shell;
    }
}