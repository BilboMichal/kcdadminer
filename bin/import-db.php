#!/bin/php
<?php

use App\ModConfig;
use Config\Bootstrap;

require __DIR__ . '/../vendor/autoload.php';

$container = Bootstrap::boot()->createContainer();

$modConfig = $container->get(ModConfig::class);

$bashEnvVars = array_filter([
    'DB_HOST' => $modConfig['wh_sys_SQLServer'],
    'DB_USER' => $modConfig['wh_sys_SQLUser'],
    'DB_PASS' => $modConfig['wh_sys_SQLPassword'],
    'DB_NAME' => $modConfig['wh_sys_SQLDatabase'],
    'MOD_DIR' => $container->getParameter('modDir'),
    'SEQUENCE_OFFSET' => $container->getParameter('sequenceOffset'),
]);

$bashPass = [];
foreach ($bashEnvVars as $env => $value) {
    $bashPass[] = $env . '=' . $value;
}

exit(system(sprintf('%s ./import-db.sh', implode(' ', $bashPass))));
