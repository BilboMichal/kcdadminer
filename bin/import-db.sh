#!/bin/sh

# https://modding.wiki/en/kingdomcomedeliverance/developers/setting-up-database

echo "Creating database $DB_NAME"
psql postgresql://$DB_USER:$DB_PASS@$DB_HOST -c "CREATE DATABASE $DB_NAME"
psql postgresql://$DB_USER:$DB_PASS@$DB_HOST/$DB_NAME -c "CREATE EXTENSION \"uuid-ossp\""

echo "Import modding..."
psql postgresql://$DB_USER:$DB_PASS@$DB_HOST/$DB_NAME -f "$MOD_DIR/Data_reference/modding.sql"
echo "Import public..."
psql postgresql://$DB_USER:$DB_PASS@$DB_HOST/$DB_NAME -f "$MOD_DIR/Data_reference/public.sql"
echo "Import sequences with offset $SEQUENCE_OFFSET..."
psql postgresql://$DB_USER:$DB_PASS@$DB_HOST/$DB_NAME -f "$MOD_DIR/Data_reference/sequences.sql" -v sequenceoffset=$SEQUENCE_OFFSET
